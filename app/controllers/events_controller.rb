class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_filter :require_admin!, except: [:index, :show]

  # GET /events
  # GET /events.json
  def index
    @events = Event.order("timeslot_id asc")
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @available_prios = [1, 2, 3]
    if user_signed_in?
      @reg = current_user.registrations.where(event_id: @event.id).first
      other_regs = current_user.registrations.joins(:event).where("events.timeslot_id = ?", @event.timeslot.id)
      @available_prios = @available_prios - other_regs.map(&:priority)
    end
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.speakers = [Speaker.find(params[:event][:speaker_id])] unless params[:event][:speaker_id].blank?

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render action: 'show', status: :created, location: @event }
      else
        format.html { render action: 'new' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        new_speaker = Speaker.find(params[:event][:speaker_id])
        @event.speakers << new_speaker unless params[:event][:speaker_id].blank? && @event.speakers.include?(new_speaker)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :start_time, :end_time, :description, :image_url, :location, :location_url, :timeslot_id)
    end
end
