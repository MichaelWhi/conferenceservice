class RegistrationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :require_admin!, only: [:index]
  
  def index
    @registrations = current_user.registrations
  end
  
  def create
    @event = Event.find params[:registration][:event_id]

    @registration = Registration.new
    @registration.event = @event
    @registration.user = current_user
    @registration.priority = params[:registration][:priority]
    if @registration.save
      redirect_to @event, notice: "Anmeldung zum Event erfolgreich."
    else
      redirect_to @event, alert: "Anmeldung zum Event nicht erfoglreich, evtl. ueberschneidet sich die Anmeldung mit einer bestehenden. Wähle eine andere Priorität oder einen anderen Timeslot."
    end
  end
  
  def update
    @registration = Registration.find params[:id]
    @registration.event = params[:registration][:event_id]
    if @registration.user == current_user && @registration.save
      redirect_to edit_user_registration_path, notice: "Anmeldung aktualisiert."
    else
        redirect_to edit_user_registration_path, alert: "Leider war die Anpassung nicht erfolgreich. Probiere es erneut."
    end
  end
  
  def destroy
    @registration = Registration.find params[:id]
    event = @registration.event
    if @registration.user == current_user && @registration.delete
      redirect_to event, notice: "Anmeldung storniert."
    else
      redirect_to event, alert: "Leider war die Stornierung nicht erfolgreich. Probiere es erneut."
    end
  end
end
