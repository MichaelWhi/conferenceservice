class SpeakersController < ApplicationController
  before_filter :require_admin!
  def index
    @speakers = Speaker.all
  end

  def show
    @speaker = Speaker.find params[:id]
  end

  def edit
    @speaker = Speaker.find params[:id]
  end

  def new
    @speaker = Speaker.new
  end

  def create
    @speaker = Speaker.new(speaker_params)
    if @speaker.save
      redirect_to @speaker, notice: "OK"
    else
      render action: :new
    end
  end

  def update
    speaker = Speaker.find params[:id]
    speaker.update speaker_params
    redirect_to speaker, notice: "OK"
  end

  def destroy
    @speaker = Speaker.find params[:id]
    @speaker.destroy
    redirect_to speakers_path
  end
  
  protected
  def speaker_params
    params.require(:speaker).permit :name, :email, :homepage
  end
end
