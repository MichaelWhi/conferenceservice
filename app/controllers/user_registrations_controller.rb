class UserRegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to root_path, notice: t("devise.confirmations.send_instructions")
    else
      render :new
    end
  end

  def update
    @user = current_user
    email_changed = @user.email != user_params[:email]
    password_changed = !user_params[:password].empty?
    successfully_updated = if email_changed or password_changed
      @user.update_with_password(user_params)
    else
      @user.update_without_password(user_params)
    end

    if successfully_updated
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, bypass: true
      redirect_to root_path, notice: t("devise.registrations.updated")
    else
      render "edit"
    end
  end
  
  def user_params
    params.require(:user).permit(:firstname, :lastname, :sex, :birthday, :street, :zip, :city, 
              :council, :university, :mobile, :email, :password, :password_confirmation, :vegetarian, :muslim, :field_bed, :comment, :arrival_via, :arrival_wednesday, :food_saturday)
  end
end