class Event < ActiveRecord::Base
  validates :name, presence: true
  has_and_belongs_to_many :speakers
  has_many :registrations, dependent: :destroy
  has_many :users, through: :registrations
  belongs_to :timeslot
  
  validates :timeslot, presence: true
  
  def overlaps?(other)
    self.timeslot.overlaps?(other.timeslot)
  end
  
  def speaker
    self.speakers.join(", ")
  end
  
  def speaker_id
    self.speakers.first.try(:id)
  end
end
