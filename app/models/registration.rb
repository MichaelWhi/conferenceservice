class Registration < ActiveRecord::Base
  belongs_to :user
  belongs_to :event
  
  validates :user_id, :event_id, presence: true
  
  validates :priority, :inclusion => {:in => 1..3}
  
  # validates_uniqueness_of :event_id, scope: :user_id
  validate :validates_non_conflicting_registrations
  
  protected
  def validates_non_conflicting_registrations
    errors.add(:event, "overlaps with already registered events.") if self.user.registrations.any? do |user_reg|
      user_reg.event.overlaps?(self.event) and user_reg.priority == self.priority
    end
  end
  
end
