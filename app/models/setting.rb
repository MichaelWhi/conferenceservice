class Setting < ActiveRecord::Base
  validates :name, uniqueness: true
  def self.get(setting_name)
    self.where(name: setting_name).first.try(:active)
  end
end
