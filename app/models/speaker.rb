class Speaker < ActiveRecord::Base
  has_and_belongs_to_many :events
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
end
