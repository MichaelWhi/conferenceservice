class Timeslot < ActiveRecord::Base
  has_many :events
  def overlaps?(other)
    (start_time - other.end_time) * (other.start_time - end_time) >= 0
  end
end
