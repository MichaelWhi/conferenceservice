class User < ActiveRecord::Base
  has_many :registrations, dependent: :destroy
  has_many :events, through: :registrations
  
  # Include default devise modules. Others available are:
  # :token_authenticatable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable, 
         :recoverable, :rememberable, :trackable, :validatable
         
  validates :firstname, :lastname, :sex, :birthday, :street, :zip, :city, 
            :council, :university, presence: true
  validates :sex, inclusion: { in: ["m", "w"] }
  validates :username, exclusion: { in: %w(admin superuser) }
  validates :zip, length: {is: 5}
  
  def name
    "#{self.firstname} #{self.lastname}"
  end
end
