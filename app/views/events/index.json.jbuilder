json.array!(@events) do |event|
  json.extract! event, :name, :start_time, :end_time, :description, :image_url, :location, :location_url
  json.url event_url(event, format: :json)
end