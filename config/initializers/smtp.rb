ActionMailer::Base.smtp_settings = {
  :address        => 'mailout.bufak-paderborn.de',
  :port           => '25',
  :authentication => :plain,
  :user_name      => ENV['SMTP_USERNAME'],
  :password       => ENV['SMTP_PASSWORD'],
  :enable_starttls_auto => false
}