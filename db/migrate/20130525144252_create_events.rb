class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.datetime :start_time
      t.datetime :end_time
      t.text :description
      t.text :image_url
      t.text :location
      t.text :location_url

      t.timestamps
    end
  end
end
