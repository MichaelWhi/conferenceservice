class AddRegistrationFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :firstname, :string
    add_column :users, :lastname, :string
    add_column :users, :sex, :string
    add_column :users, :birthday, :date
    add_column :users, :street, :string
    add_column :users, :zip, :string
    add_column :users, :city, :string
    add_column :users, :student, :boolean
    add_column :users, :council, :string
    add_column :users, :university, :string
    add_column :users, :mobile, :string
    add_column :users, :username, :string
    add_column :users, :vegetarian, :boolean
    add_column :users, :allergic, :boolean
    add_column :users, :muslim, :boolean
    add_column :users, :field_bed, :boolean
    add_column :users, :comment, :text
    add_column :users, :invoice_paid, :datetime
    add_column :users, :team_info, :string
  end
end
