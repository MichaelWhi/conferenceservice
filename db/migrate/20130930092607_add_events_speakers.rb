class AddEventsSpeakers < ActiveRecord::Migration
  TABLE = :events_speakers
  def self.up
      create_table TABLE, id: false do |t|
          t.references :event
          t.references :speaker
      end
      add_index TABLE, [:event_id, :speaker_id]
      add_index TABLE, :speaker_id
      remove_column :events, :speaker_id
    end

    def self.down
      drop_table TABLE
      add_column :events, :speaker_id
    end
end
