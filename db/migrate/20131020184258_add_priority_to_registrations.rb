class AddPriorityToRegistrations < ActiveRecord::Migration
  def change
    add_column :registrations, :priority, :integer, default: 1
  end
end
