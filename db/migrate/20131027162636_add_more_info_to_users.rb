class AddMoreInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :arrival_via, :string
    add_column :users, :arrival_wednesday, :boolean, default: false
  end
end
