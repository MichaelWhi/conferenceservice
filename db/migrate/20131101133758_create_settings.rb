class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :name
      t.boolean :active

      t.timestamps
    end
      
  end
end
