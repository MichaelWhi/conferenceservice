class CreateSignUpSetting < ActiveRecord::Migration
  class Setting < ActiveRecord::Base
  end
  
  def change
    Setting.create!(name: 'disable sign up', active: false)
  end
end
