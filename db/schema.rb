# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131106231313) do

  create_table "events", force: true do |t|
    t.string   "name"
    t.datetime "start_time"
    t.datetime "end_time"
    t.text     "description"
    t.text     "image_url"
    t.text     "location"
    t.text     "location_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.integer  "timeslot_id"
  end

  add_index "events", ["timeslot_id"], name: "index_events_on_timeslot_id"

  create_table "events_speakers", id: false, force: true do |t|
    t.integer "event_id"
    t.integer "speaker_id"
  end

  add_index "events_speakers", ["event_id", "speaker_id"], name: "index_events_speakers_on_event_id_and_speaker_id"
  add_index "events_speakers", ["speaker_id"], name: "index_events_speakers_on_speaker_id"

  create_table "rails_admin_histories", force: true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      limit: 2
    t.integer  "year",       limit: 5
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], name: "index_rails_admin_histories"

  create_table "registrations", force: true do |t|
    t.integer  "event_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "priority"
  end

  create_table "settings", force: true do |t|
    t.string   "name"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "speakers", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "homepage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "timeslots", force: true do |t|
    t.string   "name"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "sex"
    t.date     "birthday"
    t.string   "street"
    t.string   "zip"
    t.string   "city"
    t.boolean  "student"
    t.string   "council"
    t.string   "university"
    t.string   "mobile"
    t.string   "username"
    t.boolean  "vegetarian"
    t.boolean  "allergic"
    t.boolean  "muslim"
    t.boolean  "field_bed"
    t.text     "comment"
    t.datetime "invoice_paid"
    t.string   "team_info"
    t.boolean  "admin",                  default: false
    t.string   "arrival_via"
    t.boolean  "arrival_wednesday",      default: false
    t.string   "food_saturday"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
